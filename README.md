# Equipe de desenvolvimento da integraçao

## Objetivos:

- Desenvolver o CI/CD da aplicaçao

## Grupo 8 - Membros

- Paulo Henrique de Morais Santiago (Lider)
- Vinícius Amorim da Silva - amorimm.vinicius@gmail.com - desenvolvedor
- Vinicius Barreto Costa - viniiciusbk@gmail.com
- 





## Atividade proposta pelo professor.

1. Objetivo: 

* Implementar um cliente e servidor Tetrinet utilizando sockets em nodejs. 

1.1 Requisitos
* Criar ou melhorar o servidor que suporta jogar uma partida entre jogadores - server-g1
* Escrever ou melhorar documento(s) semelhante(s) a RFC - protocol-g4
* Criar e testar tutoriais - teste cruzado entre grupos tutoriais - tutorials-g2 e tutorials-g7
* Criar modelos e artefatos (diagrama de sequência e componentes) do servidor - devel-docs-g3
* Criar, melhorar e automatizar testes do servidor e do cliente desenvolvidos - tests-g5
* Criar e automatizar processos para colaborar, construir ou testar software - integration-g8
* Criar um cliente não-gráfico, na forma de biblioteca - que comunique e usufrua as funcionalidades do servidor - cliente-g6

1.2. Entregáveis Obrigatórios:

* Formatos permitidos: HTML, markdown, javascript, json, png. Não são permitidos outros tipos de formatos.
* Auto-avaliação semanal das sprints, informando os links dos commits produzidos. Observação: quem produzir documentação na wiki, pode submeter na auto-avaliação os links das páginas em que houveram maior número de contribuição, uma vez que o Gitlab não está disponibilizando os links de commits das wikis.


1.3. Entregáveis Desejáveis:

* Manual do usuário: informando como utilizar o servidor; requisitos de hardware e software; como instalar, testar e configurar;
* Manual do desenvolvedor: lista de cada mensagem suportada, referencias (RFC), testes de execução (procedimentos em telnet, resultado esperado); lista de mensagens não suportadas; diagramas de sequencia, diagramas de atividade.
* RFC do protocolo
* Software e/ou documentação de testes
* Scripts ou manuais para integração
* Tutoriais, testes e revisões dos tutoriais
* Issues e comentários 
* Artefatos de modelagem (modelos) ou documentação (as-built)

2. Ambiente e equipe

O projeto é em grupo de 5 participantes, sendo que um será o líder, responsável pela especificação, revisão e coordenação de tarefas. 

O projeto de cada grupo deve ser desenvolvido no Gitlab, no grupo tetrinetjs, dividido por projetos:

https://gitlab.com/tetrinetjs/<projetos>

3. Avaliação
A avaliação terá nota individual e nota em grupo, exclusivamente pela produção no Gitlab. Produção entregue fora do ambiente não serão considerados.

A avaliação individual será feita através de auto-avaliações semanais por sprint (opcionais) e um relatório final do projeto (obrigatório). Detalhes:
- Toda funcionalidade implementada deve referenciar os links dos commits realizados e um caso de teste correspondente;
- Os relatórios semanais servem para feedback individual;
- Cada membro deve desenvolver seu código dentro da sua própria branch;
- O líder deve estipular prazos de entrega individual e deve realizar todos os testes, bem como fazer a integração do código-fonte;

A avaliação em grupo (40% da nota) será pelo projeto entregue, seguindo os seguintes critérios:
- profundidade técnica: emprego de técnicas ou conhecimento profundo e efetivo
- criatividade: uso de soluções inovadoras ou conhecimento recente e inédito
- volume: quantidade de funcionalidades 
- corretude: livre de bugs ou falhas

Os critérios da avaliação individual (60%) vai ser realizado conforme o relatório individual final, seguindo os seguintes critérios:
- relevância: impacto positivo no desempenho do grupo
- conformidade: concordância com os padrões previamente estabelecidos
- criatividade: uso de soluções inovadoras ou conhecimento recente e inédito
- corretude: livre de bugs ou falhas

4. Cronograma
- 08/09/19  - Entrega do primeiro Sprint
- 15/09/19 - Entrega do segundo Sprint
- 22/09/19 - Entrega do terceiro Sprint
- 29/09/19 - Entrega do Projeto 1